### Javascript Enrichment

[Javascript enrichment](https://docs.snowplow.io/docs/enriching-your-data/available-enrichments/custom-javascript-enrichment/) evaluates the payload for the presence of `stm` (which corresponds to `dvce_sent_tstamp`) and `dtm`(`dvce_created_tstamp`). If they are missing, the event is deemed invalid.

the code responsible for that check is located under the `script` key in the JSON object and is base64-encoded. You can use [Base64 Encode](https://www.base64encode.org/) for encoding and [Base64 Decode](https://www.base64decode.org/) for decoding the code.

Decoded code is added in `index.js` file
