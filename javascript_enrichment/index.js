function process(event) {
  if (!event.getDvce_sent_tstamp()) {
    throw "dvce_sent_tstamp is not defined";
  }

  if (!event.getDvce_created_tstamp()) {
    throw "dvce_created_tstamp is not defined";
  }

  const dvceSentTstamp = new Date(event.getDvce_sent_tstamp());
  const dvceCreatedTstamp = new Date(event.getDvce_created_tstamp());
  const timeDifference = dvceSentTstamp.getTime() - dvceCreatedTstamp.getTime();
  const hoursDifference = timeDifference / (1000 * 60 * 60);

  if (hoursDifference > 24) {
    throw `The time difference between dvce_sent_tstamp (${dvceSentTstamp.toISOString()}) and dvce_created_tstamp (${dvceCreatedTstamp.toISOString()}) is greater than 24 hours`;
  }
}
