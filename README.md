# custom-enrichers

This repo adds snowplow custom enrichers which are to be used by [devkit](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit) and [analytics-stack](https://gitlab.com/gitlab-org/analytics-section/product-analytics/analytics-stack)
